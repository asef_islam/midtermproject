#include <stdio.h>
#include "ppmIO.h"
#include <assert.h> 
#include "imageManip.h"

/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match.
 */
int fileeq(char lhsName[], char rhsName[]) {
    FILE* lhs = fopen(lhsName, "r");
    FILE* rhs = fopen(rhsName, "r");

    // don't compare if we can't open the files
    if (lhs == NULL || rhs == NULL) {
      printf("Couldn't open\n"); 
      return 0;
    }
    int match = 1;
    // read until both of the files are done or there is a mismatch
    while (!feof(lhs) || !feof(rhs)) {
        if (feof(lhs) ||                  // lhs done first
            feof(rhs) ||                  // rhs done first
            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
            match = 0;
	    printf("Doesn't match\n"); 
            break;
        }
    }
    fclose(lhs);
    fclose(rhs);
    return match;
}

int main() {
  FILE* fp = fopen("nika.ppm","r");
  if (fp == NULL) {
    printf("File doesn't exist");
    return 1; 
  }
  assert(fp != NULL); 
  
  Image* im = readPPM(fp);
  assert (im != NULL);
  FILE* out = fopen("nikaout.ppm", "w"); 
  writePPM(out, im);
  fclose(out);
  assert(fileeq("nika_swap1.ppm", "result_files/nika-swap1.ppm"));
  assert(fileeq("nika_swap2.ppm", "result_files/nika-swap2.ppm"));
  assert(fileeq("nika_swap3.ppm", "result_files/nika-swap3.ppm"));
  assert(fileeq("nika_blackout.ppm", "result_files/nika-blackout-UPDATED.ppm"));
  assert(fileeq("nika_grayscale.ppm", "result_files/nika-grayscale.ppm"));
  assert(fileeq("nika_crop-60-25-600-565.ppm", "result_files/nika-crop_60_25_600_565.ppm"));
  assert(fileeq("nika_contrast-0.5.ppm", "result_files/nika-contrast_0.5.ppm"));
  assert(fileeq("nika_contrast-1.5.ppm", "result_files/nika-contrast_1.5.ppm"));
  assert(fileeq("nika_contrast-2.5.ppm", "result_files/nika-contrast_2.5.ppm"));
  
  /*
  // compare generated swap1 to expected swap1
  swap(im);
  FILE* swap1_out = fopen("nika-swap1.ppm", "w");
  writePPM(swap1_out, im);
  //assert(fileeq("nika-swap1.ppm", "result_files/nika-swap1.ppm"));

  // compare generated swap2 to expected swap2
  swap(im);
  FILE* swap2_out = fopen("nika-swap2.ppm", "w");
  writePPM(swap2_out, im);
  //assert(fileeq("nika-swap2.ppm", "result_files/nika-swap2.ppm"));

  // compare generated swap3 to expected swap3
  swap(im);
  FILE* swap3_out = fopen("nika-swap3.ppm", "w");
  writePPM(swap3_out, im); 
  //assert(fileeq("nika-swap3.ppm", "result_files/nika-swap3.ppm"));

  im = readPPM(fp);
  grayscale(im);
  FILE* grayscale_out = fopen("nika-grayscale.ppm", "w");
  writePPM(grayscale_out, im);
  assert(fileeq("nika-grayscale.ppm", "result_files/nika-grayscale.ppm")); 
  */
  printf("All tests passed\n"); 
  return 0;
  
}
