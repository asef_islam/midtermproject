#include "ppmIO.h"
#include <math.h>
#include "imageManip.h"

void swap(Image* image) {
  for (int i = 0; i < image->rows * image->cols; i++) { //loop through all pixels
    unsigned char temp  = image->data[i].r; //store old R in temp 
    image->data[i].r = image->data[i].g; // move old g to new r
    image->data[i].g = image->data[i].b; // move old b to new g
    image->data[i].b = temp; //move old r to new b
  }
    
 }

void blackout(Image* image) {
  int startcol = (int) ceil(image->cols/2); //what column to start at
  int endrow = (int) floor(image->rows/2); //what row to end at 
  for (int i = 0; i < endrow; i++) {
    for (int j = startcol; j < image->cols; j++) {
      image->data[i*image->cols+j].r = 0; //blackout the pixel
      image->data[i*image->cols+j].g = 0;
      image->data[i*image->cols+j].b = 0;
    }
  }
}

Image* crop(Image* image, int top_left_x, int top_left_y, int bottom_right_x, int bottom_right_y) {
  Image* cropped = malloc(sizeof(Image));
  cropped->rows = bottom_right_y - top_left_y;
  cropped->cols = bottom_right_x - top_left_x; 
  cropped->data = malloc(cropped->rows * cropped->cols * sizeof(Pixel)); //resize the data
  int p = 0; //pixel of cropped 
  for (int i = top_left_y; i < bottom_right_y; i++) {
    for (int j = top_left_x; j < bottom_right_x; j++) {
      cropped->data[p] = image->data[i*image->cols+j];
      p++;
    }
  } 
  return cropped; 
  
}

void grayscale(Image* image) {
  for (int i = 0; i < image->rows * image->cols; i++) { //loop through all pixels
    double intensity = 0.30*image->data[i].r + 0.59*image->data[i].g + 0.11*image->data[i].b;
    image->data[i].r = (unsigned char) intensity;
    image->data[i].g = (unsigned char) intensity;
    image->data[i].b = (unsigned char) intensity;
  }
}

void contrast(Image* image, double adj_fact) {
  for (int i = 0; i < image->rows * image->cols; i++) { //loop through all pixels
    image->data[i].r = adjust_contrast(image->data[i].r, adj_fact); 
    image->data[i].g = adjust_contrast(image->data[i].g, adj_fact);
    image->data[i].b = adjust_contrast(image->data[i].b, adj_fact);
  }
}

unsigned char adjust_contrast(unsigned char channel, double adj_fact) {
  double chandub = -0.5 + ((double) channel / 255.0); //to double in range [-0.5, 0.5]
  chandub *= adj_fact;
  chandub += .5;
  chandub *= 255;
  if (chandub > 255) {
    chandub = 255; //saturate
    }
  else if (chandub < 0) {
    chandub = 0;
    }
  unsigned char new_channel = (unsigned char) chandub;
  return new_channel; 
}
