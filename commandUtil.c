#include <stdio.h>
#include "commandUtil.h"
#include <stdlib.h>
#include <string.h>
#include <errno.h> 
#include "imageManip.h" 

int get_command(int argc, const char* argv[]) {
  if (argc < 3) { //one or zero arguments were inputted
    fprintf(stderr, "Error: input or output filename not supplied\n");
    return 1;
  }
  const char* infile = argv[1];
  const char* outfile = argv[2];
  FILE* in_pic = fopen(infile, "r");
  if (in_pic == NULL) { //if file could not be opened
    fprintf(stderr, "Error: input file not found\n");
    return 2;
  }
  Image* image = readPPM(in_pic); //input image is read
  fclose(in_pic); 
  if (image == NULL) { //if readPPM failed
    fprintf(stderr, "Error: input file could not be read\n");
    free(image->data);
    free(image);
    return 3;
  }
  int error_num = error_check(argc, argv); //check for write error, no operation name, too many arguments
  if(error_num != 0) { //error is found
    free(image->data);
    free(image);
    return error_num;
  }
  const char* command = argv[3];
  Image* cropped; //will be where cropped image is stored
  if (!strcmp(command, "crop")) { //if command is crop
    int crop_error = crop_check(argc, argv, image); //check for error with crop arguments
    if (crop_error != 0) {
      free(image->data);
      free(image); 
      return crop_error;
    }
    cropped = crop(image, atoi(argv[4]), atoi(argv[5]), atoi(argv[6]), atoi(argv[7]));
    free(image->data);
    free(image);    
  }
  else if (!strcmp(command, "blackout")) { //if command is blackout
    blackout(image);
  }
  else if (!strcmp(command, "swap")) { //if command is swap
    swap(image);
  }
  else if (!strcmp(command, "grayscale")) { //if command is grayscale
    grayscale(image);
  }
  else { //if command is contrast
    int contrast_error = contrast_check(argc, argv); //check for error with contrast arguments
    if (contrast_error != 0) {
      free(image->data);
      free(image);
      return contrast_error;
    }
    double adj_fact = atof(argv[4]); //string to double 
    contrast(image, adj_fact);
  } 
  if (!strcmp(command, "crop")) { //if command is crop
    return write_out(cropped,outfile);
  }
  else {
    return write_out(image,outfile);
  }
}


int error_check(int argc, const char* argv[]) {
  FILE* out = fopen(argv[2], "w");
  if (out == NULL) { //if opening output file failed
    fprintf(stderr, "Error: could not open output file\n");
    fclose(out);
    return 4;
  }
  fclose(out);
  
  if (argc == 3) { //since the two arguments inputted must be the filenames, no operation was given
    fprintf(stderr, "Error: no operation name specified\n");
    return 5;
  }

  const char* operation = argv[3];
  if(!(!strcmp(operation, "crop") || !strcmp(operation, "contrast") || !strcmp(operation, "blackout") ||
       !strcmp(operation, "grayscale") || !strcmp(operation, "swap"))) { //if operation is none of the options
    fprintf(stderr, "Error: operation not recognized\n");
    return 5;
  }

  if (!(!strcmp(operation, "contrast") || !strcmp(operation, "crop"))) { //if operation is not contrast or crop
    if (!(argc == 4)) { //there must be four arguments (three inputted arguments)
      fprintf(stderr, "Error: Incorrect number of arguments\n");
      return 6;
    }
  }
  return 0;
}


int crop_check(int argc, const char* argv[], Image* image) {
  if (argc != 8) { //crop requires 8 arguments
     fprintf(stderr, "Error: expecting 8 arguments for crop\n");
     return 6;
  }
  else {
    char* top_left_x_error;
    char* top_left_y_error; 
    long top_left_x = strtol(argv[4], &top_left_x_error, 10); // string to int
    long top_left_y = strtol(argv[5], &top_left_y_error, 10);
    if (!strcmp(top_left_x_error, argv[4]) || *top_left_x_error != '\0'
	|| !strcmp(top_left_y_error, argv[5]) || *top_left_y_error != '\0') { //error
      fprintf(stderr, "Error: Crop arguments not valid integers\n"); 
      return 6;
    }
    int bottom_right_x = atoi(argv[6]);
    int bottom_right_y = atoi(argv[7]);
    if (!(top_left_x >= 0 && top_left_x <= image->cols && top_left_y >= 0 && top_left_y <= image->rows
          && bottom_right_x >= top_left_x + 1 && bottom_right_x <= image->cols + 1
          && bottom_right_y >= top_left_y + 1 && bottom_right_y <= image->rows + 1)) { //if any crop arguments out of range
      fprintf(stderr, "Error: crop arguments out of range for this image\n");
      return 7;
    }
  }
  return 0;
}


int contrast_check(int argc, const char* argv[]) {
  if (argc != 5) { //five arguments should be found
    fprintf(stderr, "Error: expecting 5 arguments for contrast\n");
    return 6;
  }
  else {
    char* adj_fact_error;
    double adj_fact = strtod(argv[4], &adj_fact_error);
    if (!strcmp(adj_fact_error, argv[4]) || *adj_fact_error != '\0') { //error
      fprintf(stderr, "Error: adjustment factors is not a valid double\n");
      return 6;
    }
  }
}
    
  
int write_out(Image* out_image, const char* outfile) {
  FILE* out_pic = fopen(outfile, "w");
  if (out_pic == NULL) { //if opening output file failed
    fprintf(stderr, "Error: could not open output file\n");
    fclose(out_pic);//outfile is closed
    free(out_image->data);
    free(out_image);
    return 4;
  }

  if (writePPM(out_pic,out_image) == -1) { //if writePPM failed to write new image to outfile
    fprintf(stderr, "Error: writing output image failed\n");
    fclose(out_pic); //outfile is closed
    free(out_image->data);
    free(out_image);
    return 4;
  }
  fclose(out_pic); //outfile is closed
  free(out_image->data);
  free(out_image); 
  return 0;
}
