# Lines starting with # are comments

# Some variable definitions to save typing later on
CC = gcc
CONSERVATIVE_FLAGS = -std=c99 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

#Links the project.c executable
project: project.o ppmIO.o commandUtil.o imageManip.o
	$(CC) -o project project.o ppmIO.o commandUtil.o imageManip.o

# Links the test executable
test: test.o ppmIO.o commandUtil.o imageManip.o
	$(CC) -o test test.o ppmIO.o commandUtil.o imageManip.o

# Compiles project.c into an object file
project.o: project.c ppmIO.h commandUtil.h imageManip.h 
	$(CC) $(CFLAGS) -c project.c

# Compiles test.c into an object file
test.o: test.c ppmIO.h commandUtil.h imageManip.h
	$(CC) $(CFLAGS) -c test.c

# Compiles commandUtil.c into an object file
commandUtil.o: commandUtil.c ppmIO.h imageManip.h 
	$(CC) $(CLAGS) -c commandUtil.c

# Compiles ppmIO.c into an object file
ppmIO.o: ppmIO.c ppmIO.h
	$(CC) $(CLAGS) -c ppmIO.c

# Compiles imageManip.c into an object file
imageManip.o: imageManip.c imageManip.h
	$(CC) $(CLAGS) -c imageManip.c

# 'make clean' will remove intermediate & executable files
clean:
	rm -f *.o test project *.gcov
