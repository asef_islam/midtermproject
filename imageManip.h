#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

#include "ppmIO.h"



void swap(Image* image);

void blackout(Image* image);

Image* crop(Image* image, int top_left_x, int top_left_y, int bottom_right_x, int bottom_right_y);

void grayscale(Image* image);

void contrast(Image* image, double adj_fact);

unsigned char adjust_contrast(unsigned char channel, double adj_fact);  

#endif
