//ppmIO.c
//601.220, Spring 2018
//Starter code for midterm project - feel free to edit/add to this file

#include <stdio.h>
#include "ppmIO.h"
#include <string.h>
#include <stdlib.h>

/* read PPM formatted image from a file (assumes fp != NULL) */
Image* readPPM(FILE *fp) {
  char tag[3]; 
  fgets(tag, 3, fp);
  if (!(strcmp(tag, "P6") == 0)) { //should be P6 
    fprintf(stderr, "Error: Could not read P6 tag\n");
    return NULL;  
  }
  char com_start;
  fscanf(fp, " %c", &com_start);
  if (com_start == '#') { //if next line is a comment, (can have up to 1 comment line)
    char comment[5000]; 
    fgets(comment, 5000, fp);
  }
  else { //put it back 
    ungetc(com_start, fp);
  }
  Image* pic = malloc(sizeof(Image)); 
  fscanf(fp, "%d", &pic->cols);
  fscanf(fp, "%d", &pic->rows);
  int max_color;
  fscanf(fp, "%d", &max_color);
  fgetc(fp); 
  if (max_color != 255) {
    fprintf(stderr, "Error: Maximum color value should be 255\n");
    return NULL;
  }
  long num_pix = pic->rows * pic->cols; 
  pic->data = malloc(num_pix * sizeof(Pixel));
  size_t num_read = fread(pic->data, sizeof(Pixel), num_pix, fp);
  if ((int) num_read != num_pix) {
    fprintf(stderr, "Error: Unexpected read result\n");
    return NULL;
  }
  return pic;
}




/* write PPM formatted image to a file (assumes fp != NULL and img != NULL) */
int writePPM(FILE *fp, const Image* im) {

  /* abort if either file pointer is dead or image pointer is null; indicate failure with -1 */
  if (!fp || !im) {
    return -1;
  }

  /* write tag and dimensions; colors is always 255 */
  fprintf(fp, "P6\n%d %d\n%d\n", im->cols, im->rows, 255);

  /* write pixels */
  int num_pixels_written = (int) fwrite(im->data, sizeof(Pixel), (im->rows) * (im->cols), fp);

  /* check if write was successful or not; indicate failure with -1 */
  if (num_pixels_written != (im->rows) * (im->cols)) {
    return -1;
  }

  /* success, so return number of pixels written */
  return num_pixels_written;
}



