#ifndef COMMANDUTIL_H
#define COMMANDUTIL_H 

#include "ppmIO.h"

int get_command(int argc, const char* argv[]); 

int error_check(int argc, const char* argv[]);

int write_out(Image* out_image, const char* outfile);

int crop_check(int argc, const char* argv[], Image* image);

int contrast_check(int argc, const char* argv[]); 

#endif
